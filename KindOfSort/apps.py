from django.apps import AppConfig


class KindofsortConfig(AppConfig):
    name = 'KindOfSort'
