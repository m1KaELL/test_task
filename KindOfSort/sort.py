from abc import ABC, abstractmethod
from random import randint
import operator
import time
import re

class Sort(ABC):

    #decorator, which measures executes time for a function
    def view_function_timer(prefix='', writeto=print):

        def decorator(func):
            def wrapper(*args, **kwargs):
                try:
                    t0 = time.time()
                    return func(*args, **kwargs)
                finally:
                    t1 = time.time()
                    writeto(
                        'View Function',
                        '({})'.format(prefix) if prefix else '',
                        func.__name__,
                        args[1:],
                        'Took',
                        '{:.2f}ms'.format(1000 * (t1 - t0)),
                    )
            return wrapper

        return decorator

    #variable decloration
    def __init__(self,file_name):
        self.file_name=file_name
        self.data = []
        with open(self.file_name,'r') as fil:
            for line in fil:
                self.data.append([int(x) for x in re.split(r"[ ,.:?&!/]",line)])
            fil.close()
        
    @abstractmethod
    def sort(self,file_name):
        pass

class BubbleSort(Sort):

    @Sort.view_function_timer()
    #method for bubble sort
    def sort(self):
        for a in range(len(self.data)):
            for i in range(len(self.data[a-1])-1):
                for j in range(len(self.data[a-1])-i-1):
                    if self.data[a-1][j] > self.data[a-1][j+1]:
                        self.data[a-1][j], self.data[a-1][j+1] = self.data[a][j+1], self.data[a][j]
            return self.data

class InsertionSort(Sort):
    
    @Sort.view_function_timer()
    #method for insertion sort
    def sort(self):
        for a in range(len(self.data)):
            for i in range(len(self.data[a-1])):
                ins = self.data[a-1][i]
                j = i-1
                while j >=0 and ins < self.data[a-1][j] :
                    self.data[a-1][j+1] = self.data[a-1][j]
                    j -= 1
                self.data[a-1][j+1] = ins 
        return self.data

class MergeSort(Sort):
    
    @Sort.view_function_timer()
    #method for merge sort
    def sort(self,a):
        if len(a) < 2:
            return a

        left = MergeSort.sort(self,a[:len(a)//2])
        right = MergeSort.sort(self,a[len(a)//2:len(a)])

        i,j = 0,0
        result = []
        while i < len(left) or j < len(right):
            if not i < len(left):
                result.append(right[j])
                j += 1
            elif not j < len(right):
                result.append(left[i])
                i += 1
            elif left[i] < right[j]:
                result.append(left[i])
                i += 1
            else:
                result.append(right[j])
                j += 1

        return result


