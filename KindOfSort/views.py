from django.shortcuts import render
from KindOfSort.forms import SortForm
from KindOfSort.sort import *
from KindOfSort.models import Sort
import re

def index(request):

    form = SortForm()
    if request.method == "POST":
        #bubble sort for file.txt
        if request.POST.get('name') == "bubble":
            sort=BubbleSort("unsorted_arrays/"+request.POST.get('unsorted_array'))
            sotred_array = sort.sort()
            new_file_sorted = Sort.objects.create(name=request.POST.get('name'),unsorted_array=request.POST.get('unsorted_array'),sorted_array=sotred_array)
            new_file_sorted.save()

        #insertion sort for file.txt
        if request.POST.get('name') == "insertion":
            sort=InsertionSort("unsorted_arrays/"+request.POST.get('unsorted_array'))
            sotred_array = sort.sort()
            new_file_sorted = Sort.objects.create(name=request.POST.get('name'),unsorted_array=request.POST.get('unsorted_array'),sorted_array=sotred_array)
            new_file_sorted.save()

        #merge sort for file.txt
        if request.POST.get('name') == "merge":
            sort=MergeSort("unsorted_arrays/"+request.POST.get('unsorted_array'))
            data = []
            with open ("unsorted_arrays/"+request.POST.get('unsorted_array')) as fil:
                for line in fil:
                    data.append([int(x) for x in re.split(r"[ ,.:?&!/]",line)])
            sorted_array = []
            for i in range(len(data)):
                sorted_string = sort.sort(data[i])
                sorted_array.append(sorted_string) 
            new_file_sorted = Sort.objects.create(name=request.POST.get('name'),unsorted_array=request.POST.get('unsorted_array'),sorted_array=sorted_array)
            new_file_sorted.save()

    context = {
        "form":form
    }
    return render(request,'base.html',locals())
