from django.db import models

class Sort(models.Model):
    TYPE_SORT = [
        ('bubble','BUBBLE'),
        ('insertion','INSERTION'),
        ('merge','MERGE'),
    ]
    name = models.CharField(max_length=30, blank=True, null=True,default=None,choices=TYPE_SORT)
    unsorted_array = models.FileField(upload_to='',default=None)
    sorted_array = models.CharField(max_length=130, blank=True, null=True,default=None)
    
    def __str__(self):
        return "Name of sort: %s " % self.name

    class Meta:
        verbose_name = "sort"
        verbose_name_plural = "sorts"