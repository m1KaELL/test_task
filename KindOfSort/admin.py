from django.contrib import admin
from KindOfSort.models import *

class SortAdmin(admin.ModelAdmin):

    list_display = [field.name for field in Sort._meta.fields]
    list_filter = ['name']
    search_fields = ['name','unsorted_array','sorted_array']
    ordering = ['name','unsorted_array','sorted_array']
    class Meta:
        model = Sort

admin.site.register(Sort,SortAdmin)