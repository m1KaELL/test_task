from django import forms
from KindOfSort.models import Sort

#TYPE_SORT = [
#    ('bubble','BUBBLE'),
#    ('insertion','INSERTION'),
#    ('merge','MERGE'),
#]
class SortForm(forms.ModelForm):
    #type_sort = forms.ChoiceField(choices=TYPE_SORT)
    class Meta:
        model = Sort
        fields = [
            "name",
            "unsorted_array",
        ]
        