# test_task
# Prerequisites
- **Python 3.6+.**
- **Django 3+.**
- **Pillow 5.0+.**

- Please check your python version with the following command. The result should be 3.6 or higher.

python3 --version

# Work in virtualenv
- **sudo -H pip install virtualenv**
- **python3 -m venv name_of_virtualenv**
- **source name_of_virtualenv/bin/activate**
pip3 install -r requirements.txt

# Run
**From folder mysite**
- **python manage.py runserver**
